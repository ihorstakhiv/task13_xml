package parser;

import comparator.CandyComparator;
import filechecker.ExtensionChecker;
import model.Candy;
import parser.dom.DOMParserUser;
import parser.sax.SAXParserUser;
import parser.stax.StAXReader;
import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {

    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\xml\\candyXML.xml");
        File xsd = new File("src\\main\\resources\\xml\\candyXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(SAXParserUser.parseBeers(xml, xsd), "SAX");
            printList(StAXReader.parseBeers(xml, xsd), "StAX");
            printList(DOMParserUser.getBeerList(xml, xsd), "DOM");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Candy> candies, String parserName) {
        Collections.sort(candies, new CandyComparator());
        System.out.println(parserName);
        for (Candy candy : candies) {
            System.out.println(candy);
        }
    }
}
