package comparator;

import model.Candy;
import java.util.Comparator;

public class CandyComparator implements Comparator<Candy> {
    @Override
    public int compare(Candy o1, Candy o2) {
        return Double.compare(o1.getChars().getAlcFraction(), o2.getChars().getAlcFraction());
    }
}
